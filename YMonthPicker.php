<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of YMonthPicker
 *
 * @author nurcahyo
 */
class YMonthPicker extends CInputWidget
{
	private $_asset;
	public $callback;
	public $monthFormat = 'MM/dd/yyyy';
	public $unixTime = false;
	public $containerHtmlOptions;

	public function init()
	{
		$this->resolveNameID();
		if ($this->hasModel() && $this->value === null)
		{
			$attribute = $this->attribute;
			$this->value = $this->model->$attribute;
		}
		$this->registerClientScript();
	}

	public function parseCurrentDateTime($time)
	{
		if (!$this->unixTime)
		{
			$parse = new CDateTimeParser();
			return $parse->parse($time, $this->monthFormat);
		}
		return $time;
	}

	public function getMonth()
	{
		$time = $this->parseCurrentDateTime($this->value);
		return "'" . Yii::app()->dateFormatter->format('yyyy-MM-dd', $time) . "'";
	}

	public function registerClientScript()
	{

		$cs = Yii::app()->clientScript;
		$asset = $this->asset;
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile("$asset/monthpicker.min.js", CClientScript::POS_END);
		$cs->registerCssFile("$asset/monthpicker.css");
		if ($this->callback != null)
			$script = "$('#$this->id').monthpicker({$this->month}," . CJavaScript::encode($this->callback) . ")";
		else
			$script = "$('#$this->id').monthpicker({$this->month})";
		$cs->registerScript(__CLASS__ . '#' . $this->id, $script, CClientScript::POS_READY);
		if ($this->value === null || trim($this->value === ''))
		{
			$cs->registerScript(__CLASS__ . '#' . $this->id . 'isNull', "
				$.li=$('#$this->id	.li a');
				$.li.on('click',function(){ 
					var year=$($.li[0].children[0]);
					year.addClass('selected');
					year.html($(this).attr('title'));
				});
				$.selected=$('#$this->id .selected');
				$.selected.removeClass('selected');"
			);
		}
	}

	public function run()
	{
		if (isset($this->containerHtmlOptions['class']))
			$this->containerHtmlOptions['class'] = 'MonthPicker ' . $this->containerHtmlOptions['class'];
		else
			$this->containerHtmlOptions['class'] = 'MonthPicker';
		$this->htmlOptions['value'] = $this->value;
		echo CHtml::openTag('div', CMap::mergeArray($this->containerHtmlOptions, array('id' => $this->id)));
		if (is_object($this->model))
		{
			echo CHtml::activeLabelEx($this->model, $this->attribute, $this->htmlOptions);
			echo CHtml::activeHiddenField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo CHtml::label($this->name, $this->name);
			echo CHtml::hiddenField($this->name, $this->value, $this->htmlOptions);
		}
		echo CHtml::closeTag('div');
	}

//	public function __call($name, $parameters)
//	{
//		if (!method_exists($this, $name))
//		{
//			if (method_exists(new CHtml, $name))
//			{
//				return call_user_func_array(array(new CHtml, $name), $parameters);
//			}
//		}
//		
//		parent::__call($name, $parameters);
//	}

	public function getAsset()
	{
		if ($this->_asset === NULL)
		{
			$assets = sprintf('%s%s%s', dirname(__FILE__), DIRECTORY_SEPARATOR, 'assets');
			$this->_asset = $assets;
		}
		return Yii::app()->assetManager->publish($this->_asset);
	}

	public function setAsset($asset = null)
	{
		$this->_asset = $asset;
	}
}

?>
